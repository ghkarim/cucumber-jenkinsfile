package com.e2etests.automation.step_definitions;

import org.junit.Assert;

import com.e2etests.automation.page_objects.AuthentificationPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class AuthentificationStepDefiniton {

	private AuthentificationPage authentificationPage;

	public AuthentificationStepDefiniton() {
		this.authentificationPage = new AuthentificationPage();
	}

	@Given("Je me connecte sur l application Mercury")
	public void jeMeConnecteSurLApplicationMercury() throws InterruptedException {
		Thread.sleep(30000);
		authentificationPage.goToUrl();
		Thread.sleep(9000);
	}

	@When("Je saisie le username {string}")
	public void jeSaisieLeUsername(String name) throws InterruptedException {
		Thread.sleep(20000);
		authentificationPage.fillUserName(name);
		Thread.sleep(20000);
	}

	@When("Je saisie password {string}")
	public void jeSaisiePassword(String password) {
		authentificationPage.fillPassword(password);
	}

	@When("Je clique sur le boutton submit")
	public void jeCliqueSurLeBouttonSubmit() {
		authentificationPage.clickOnBtnSubmit();
	}

	@Then("Je me redirige vers la page home {string}")
	public void jeMeRedirigeVersLaPageHome(String text) {
		String message = AuthentificationPage.welcomeMessage.getText();
		Assert.assertEquals(text, message);
	

	}

}
